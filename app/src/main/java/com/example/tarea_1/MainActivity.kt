package com.example.tarea_1

import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<Button>(R.id.DO1).setOnTouchListener(
            object : View.OnTouchListener {
                override fun onTouch(view: View, event: MotionEvent): Boolean {
                    when (event.action) {
                        MotionEvent.ACTION_DOWN -> {
                            DO1()
                        }
                    }
                    return false
                }
            }
        )


    }

    fun DO1(){
        val mp = MediaPlayer.create(this, R.raw.re)
        mp.start()
        mp.setOnCompletionListener(object : MediaPlayer.OnCompletionListener {
            override fun onCompletion(mp: MediaPlayer) {
                mp.release()
            }
        })
    }
    /*fun DO1(event: MotionEvent): Boolean{
        if (event.action==MotionEvent.ACTION_DOWN){
            val mp = MediaPlayer.create(this, R.raw.do1)
            mp.start()
            mp.setOnCompletionListener(object : MediaPlayer.OnCompletionListener {
                override fun onCompletion(mp: MediaPlayer) {
                    mp.release()
                }
            })
            return true
        }
        return true
    }*/

    fun RE(view: View) {
        val mp = MediaPlayer.create(this, R.raw.re)
        mp.start()
        mp.setOnCompletionListener(object : MediaPlayer.OnCompletionListener {
            override fun onCompletion(mp: MediaPlayer) {
                mp.release()
            }
        })
    }
    fun MI(view: View) {
        val mp = MediaPlayer.create(this, R.raw.mi)
        mp.start()
        mp.setOnCompletionListener(object : MediaPlayer.OnCompletionListener {
            override fun onCompletion(mp: MediaPlayer) {
                mp.release()
            }
        })
    }
    fun FA(view: View) {
        val mp = MediaPlayer.create(this, R.raw.fa)
        mp.start()
        mp.setOnCompletionListener(object : MediaPlayer.OnCompletionListener {
            override fun onCompletion(mp: MediaPlayer) {
                mp.release()
            }
        })
    }
    fun SOL(view: View) {
        val mp = MediaPlayer.create(this, R.raw.sol)
        mp.start()
        mp.setOnCompletionListener(object : MediaPlayer.OnCompletionListener {
            override fun onCompletion(mp: MediaPlayer) {
                mp.release()
            }
        })
    }
    fun LA(view: View) {
        val mp = MediaPlayer.create(this, R.raw.la)
        mp.start()
        mp.setOnCompletionListener(object : MediaPlayer.OnCompletionListener {
            override fun onCompletion(mp: MediaPlayer) {
                mp.release()
            }
        })
    }
    fun SI(view: View) {
        val mp = MediaPlayer.create(this, R.raw.si)
        mp.start()
        mp.setOnCompletionListener(object : MediaPlayer.OnCompletionListener {
            override fun onCompletion(mp: MediaPlayer) {
                mp.release()
            }
        })
    }
    fun DO2(view: View) {
        val mp = MediaPlayer.create(this, R.raw.do2)
        mp.start()
        mp.setOnCompletionListener(object : MediaPlayer.OnCompletionListener {
            override fun onCompletion(mp: MediaPlayer) {
                mp.release()
            }
        })
    }
}
